#!/bin/bash

export USERNAME_DIR=`whoami`
export TO=$1
export INSTANCE=$2
echo "Copying the users, local and lookup data of non-default apps from $INSTANCE"

sudo -E su  splunk <<'EOF'
mkdir /opt/splunk/tmp/$TO
find /opt/splunk/etc/apps -iname "local" > /opt/splunk/tmp/$TO/local.txt
sed 's/\/opt\/splunk\/etc\/apps\///g ; s/\/local//g' -i /opt/splunk/tmp/$TO/local.txt
for APP in $(cat /opt/splunk/tmp/$TO/local.txt)
do
case $APP in
075-cloudworks)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
100-s2-config)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
100-whisper)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
100-whisper-common)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
100-whisper-indexer)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
alert_logevent)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
alert_webhook)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
appsbrowser)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
cloud_administration)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
dmc)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
introspection_generator_addon)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
journald_input)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
launcher)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
learned)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
legacy)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
prometheus)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
python_upgrade_readiness_app)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
sample_app)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
scsaudit)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
search)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
search_artifacts_helper)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunk-dashboard-studio)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunk_gdi)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunk_httpinput)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunk_instrumentation)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunk_internal_metrics)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunk_metrics_workspace)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunk_product_guidance)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunk_rapid_diag)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunk_secure_gateway)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
user-prefs)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
100-cloudworks-wlm)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
100-whisper-searchhead)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
data_manager)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
dynamic-data-self-storage-app)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunkclouduf)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunk_instance_monitoring)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
tos)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
missioncontrol)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
000-self-service)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunk_ta_data_manager)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
splunk_datasets_addon)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
framework)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
_cluster_admin)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
_cluster)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/local.txt
  ;;
esac
done

sed -i '/^$/d' /opt/splunk/tmp/$TO/local.txt

mkdir /opt/splunk/tmp/$TO/local_data

for file in $(cat /opt/splunk/tmp/$TO/local.txt)
do
mkdir /opt/splunk/tmp/$TO/local_data/$file
cp -pR /opt/splunk/etc/apps/$file/local /opt/splunk/tmp/$TO/local_data/$file/
done
tar -czf /opt/splunk/tmp/$TO/${INSTANCE}_appLocals.tgz -C /opt/splunk/tmp/$TO/ local_data/
EOF

sudo -E su  splunk <<'EOF'
find /opt/splunk/etc/apps -iname "lookups" > /opt/splunk/tmp/$TO/lookups.txt
sed 's/\/opt\/splunk\/etc\/apps\///g ; s/\/lookups//g' -i /opt/splunk/tmp/$TO/lookups.txt
for APP in $(cat /opt/splunk/tmp/$TO/lookups.txt)
do
case $APP in
075-cloudworks)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
100-s2-config)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
100-whisper)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
100-whisper-common)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
100-whisper-indexer)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
alert_logevent)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
alert_webhook)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
appsbrowser)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
cloud_administration)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
dmc)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
introspection_generator_addon)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
journald_input)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
launcher)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
learned)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
legacy)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
prometheus)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
python_upgrade_readiness_app)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
sample_app)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
scsaudit)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
search)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
search_artifacts_helper)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunk-dashboard-studio)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunk_gdi)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunk_httpinput)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunk_instrumentation)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunk_internal_metrics)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunk_metrics_workspace)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunk_product_guidance)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunk_rapid_diag)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunk_secure_gateway)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
user-prefs)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
100-cloudworks-wlm)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
100-whisper-searchhead)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
data_manager)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
dynamic-data-self-storage-app)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunkclouduf)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunk_instance_monitoring)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
tos)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
missioncontrol)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
000-self-service)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunk_ta_data_manager)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
splunk_datasets_addon)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
framework)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
_cluster_admin)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
_cluster)
  sed -i "/$APP/d" /opt/splunk/tmp/$TO/lookups.txt
  ;;
esac
done

sed -i '/^$/d' /opt/splunk/tmp/$TO/lookups.txt

mkdir /opt/splunk/tmp/$TO/lookups_data

for file in $(cat /opt/splunk/tmp/$TO/lookups.txt)
do
mkdir /opt/splunk/tmp/$TO/lookups_data/$file
cp -pR /opt/splunk/etc/apps/$file/lookups /opt/splunk/tmp/$TO/lookups_data/$file/
done
tar -czf /opt/splunk/tmp/$TO/${INSTANCE}_appLookups.tgz -C /opt/splunk/tmp/$TO/ lookups_data/
EOF

sudo -E su  splunk <<'EOF'
cp -pR /opt/splunk/etc/users /opt/splunk/tmp/$TO/
tar -czf /opt/splunk/tmp/$TO/${INSTANCE}_usersList.tgz -C /opt/splunk/tmp/$TO/ users/
EOF

sudo -E su root <<'EOF'
cp -pR /opt/splunk/tmp/$TO /home/$USERNAME_DIR/
chown -R $USERNAME_DIR:$USERNAME_DIR /home/$USERNAME_DIR/$TO
EOF
