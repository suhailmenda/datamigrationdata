#!/bin/bash
echo "Enter Instance FQDN"
read INSTANCE
echo "Enter the TO Number"
read TO


if [ -z "$INSTANCE" ]
then
  echo "Instance name not provided!! Aborting..."
  exit 1
fi

if [ -z "$TO" ]
then
  echo "Ticket Number not provided!! Aborting..."
  exit 1
fi

bash -c "ssh $INSTANCE 'bash -s' < sourceToLocal.sh $TO $INSTANCE"
scp  $INSTANCE:/home/$USER/$TO/*.tgz .

echo "Do you want to proceed with uploading these packages to destination stack?(Y/N)"
read ans
echo $ans
if ! [[ $ans = "Y" || $ans = "y" ]] ; then
    echo "Exiting..."
    exit 0
fi

echo "Enter Destination instance's FQDN"
read DEST_INST
echo "Do you want to merge common users"
read common

scp *.tgz $DEST_INST:/home/$USER
bash -c "ssh $DEST_INST 'bash -s' < localToDestination.sh $TO $DEST_INST $common" 

