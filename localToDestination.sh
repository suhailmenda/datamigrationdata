export USERNAME_DIR=`whoami`
export TO="TO-12345"
export INSTANCE=$2
export DEST_INST=$1
export noncommon=$2
export common=$3


extractTar() {
if [ ! -d /opt/splunk/tmp/$TO/ ]; then
sudo -E su root <<'EOF'
mkdir /opt/splunk/tmp/$TO
cp -pR /home/$USERNAME_DIR/${INSTANCE}_appLocals.tgz  /opt/splunk/tmp/$TO/
cp -pR /home/$USERNAME_DIR/${INSTANCE}_appLookups.tgz  /opt/splunk/tmp/$TO/
cp -pR /home/$USERNAME_DIR/${INSTANCE}_usersList.tgz  /opt/splunk/tmp/$TO/
chown -R splunk:splunk /opt/splunk/tmp/$TO
EOF

sudo -E su splunk <<'EOF'
tar xzf /opt/splunk/tmp/$TO/*.tgz
EOF

elif [ ! -d /opt/splunk/tmp/$TO/local_data ]; then
sudo -E su splunk <<'EOF'
tar xzf /opt/splunk/tmp/$TO/${INSTANCE}_appLocals.tgz
EOF

elif [ ! -d /opt/splunk/tmp/$TO/lookups_data ]; then
sudo -E su splunk <<'EOF'
tar xzf /opt/splunk/tmp/$TO/${INSTANCE}_appLookups.tgz
EOF

elif [ ! -d /opt/splunk/tmp/$TO/users ]; then
sudo -E su splunk <<'EOF'
tar xzf /opt/splunk/tmp/$TO/${INSTANCE}_usersList.tgz
EOF

fi
}

copyNonCommonUsers() {
extractTar
sudo -E su splunk <<'EOF'
#tar xzf /opt/splunk/tmp/$TO/*.tgz
comm -12 <(ls /opt/splunk/tmp/$TO/users/) <(ls /opt/splunk/etc/users/) > /opt/splunk/tmp/$TO/usersCommon.txt
#echo "Common users present on both stacks"
#echo "List of users which are common"
#cat /opt/splunk/tmp/$TO/usersCommon.txt
ls /opt/splunk/tmp/$TO/users/ > /opt/splunk/tmp/$TO/totalUser.txt
#echo "Total Users"
#cat /opt/splunk/tmp/$TO/totalUser.txt
for USER in $(cat /opt/splunk/tmp/$TO/usersCommon.txt)
do
sed -i "/$USER/d" /opt/splunk/tmp/$TO/totalUser.txt
done
for USER in $(cat /opt/splunk/tmp/$TO/totalUser.txt)
do
cp -pR /opt/splunk/tmp/$TO/users/$USER /opt/splunk/etc/users/
done
echo "All the non common users are copied to destination stack"
EOF
}


mergeCommonUsers() {
extractTar
sudo -E su splunk <<'EOF'
comm -12 <(ls /opt/splunk/tmp/$TO/users/) <(ls /opt/splunk/etc/users/) > /opt/splunk/tmp/$TO/usersCommon.txt
echo "Common users present on both stacks"
echo "List of users which are common"
cat /opt/splunk/tmp/$TO/usersCommon.txt
for USER in $(cat /opt/splunk/tmp/$TO/usersCommon.txt)
do
cp -pR /opt/splunk/tmp/$TO/users/$USER /opt/splunk/etc/users/
done
EOF
}

copyLocalDataAll() {
extractTar
sudo -E su splunk <<'EOF'
for APP in $(cat /opt/splunk/$TO/local_data/)
do
cp -pR /opt/splunk/tmp/$TO/local_data/$APP/local /opt/splunk/etc/apps/local
done
EOF  
}

copyLocalDataNonCommon() {
extractTar
sudo -E su splunk <<'EOF'
for APP in $(cat /opt/splunk/$TO/local_data/)
do
cp -pR /opt/splunk/tmp/$TO/local_data/$APP/local /opt/splunk/etc/apps/local
done
EOF
}

listCommonLocal() {
extractTar
sudo -E su splunk <<'EOF'
local=$(find /opt/splunk/etc/apps/ -iname "local")
echo "Following apps have local folders on destination stack"
for APP in $(echo $local | sed 's/\/opt\/splunk\/etc\/apps\///g ; s/\/local//g' )
do
        echo $APP
done

EOF
}

if  [[ $noncommon = "Y" || $noncommon = "y" ]] ; then
    copyNonCommonUsers
fi

